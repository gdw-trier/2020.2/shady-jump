﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllDictionaries : MonoBehaviour
{
    public Dictionary<string, string> dictionaryKeyboard = new Dictionary<string, string>();
    public Dictionary<string, string> dictionaryMouse = new Dictionary<string, string>();
    // Start is called before the first frame update
    void Awake()
    {
        dictionaryKeyboard.Add("a", "<Keyboard>/a"); 
        dictionaryKeyboard.Add("b", "<Keyboard>/b"); 
        dictionaryKeyboard.Add("c", "<Keyboard>/c"); 
        dictionaryKeyboard.Add("d", "<Keyboard>/d"); 
        dictionaryKeyboard.Add("e", "<Keyboard>/e"); 
        dictionaryKeyboard.Add("f", "<Keyboard>/f"); 
        dictionaryKeyboard.Add("g", "<Keyboard>/g"); 
        dictionaryKeyboard.Add("h", "<Keyboard>/h"); 
        dictionaryKeyboard.Add("i", "<Keyboard>/i"); 
        dictionaryKeyboard.Add("j", "<Keyboard>/j"); 
        dictionaryKeyboard.Add("k", "<Keyboard>/k"); 
        dictionaryKeyboard.Add("l", "<Keyboard>/l"); 
        dictionaryKeyboard.Add("m", "<Keyboard>/m"); 
        dictionaryKeyboard.Add("n", "<Keyboard>/n"); 
        dictionaryKeyboard.Add("o", "<Keyboard>/o"); 
        dictionaryKeyboard.Add("p", "<Keyboard>/p");
        dictionaryKeyboard.Add("q", "<Keyboard>/q"); 
        dictionaryKeyboard.Add("r", "<Keyboard>/r"); 
        dictionaryKeyboard.Add("s", "<Keyboard>/s"); 
        dictionaryKeyboard.Add("t", "<Keyboard>/t"); 
        dictionaryKeyboard.Add("u", "<Keyboard>/u"); 
        dictionaryKeyboard.Add("v", "<Keyboard>/v"); 
        dictionaryKeyboard.Add("w", "<Keyboard>/w"); 
        dictionaryKeyboard.Add("x", "<Keyboard>/x"); 
        dictionaryKeyboard.Add("y", "<Keyboard>/y"); 
        dictionaryKeyboard.Add("z", "<Keyboard>/z"); 
        
        dictionaryMouse.Add("left", "<Mouse>/leftButton"); 
        dictionaryMouse.Add("right", "<Mouse>/rightButton"); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
