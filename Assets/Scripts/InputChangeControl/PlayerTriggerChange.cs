﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Transactions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.SceneManagement;

public class PlayerTriggerChange : MonoBehaviour
{
    private InputAction myActionMove;
    private InputAction myActionFreeze;
    private InputAction myActionJump;
    private InputAction myActionRespawn;
    private Dictionary<string,string> dK;
    private Dictionary<string,string> mK;
    public EventHandler MyHandler = delegate {  };
    public event Action ControlChanged = delegate {  };
    public int whichTrigger = 1;

    private InputActionMap myActionMap ;

    public int counterChange = 1;
    // Start is called before the first frame update
    void Start()
    {
        myActionMap = GetComponent<PlayerInput>().currentActionMap;
        myActionMove = GetComponent<PlayerInput>().currentActionMap.FindAction("Move");
        myActionJump = GetComponent<PlayerInput>().currentActionMap.FindAction("Jump");
        myActionFreeze = GetComponent<PlayerInput>().currentActionMap.FindAction("FreezeShadow");
        myActionRespawn = GetComponent<PlayerInput>().currentActionMap.FindAction("ShadowRespawn");
        
        dK = GetComponent<AllDictionaries>().dictionaryKeyboard;
        mK = GetComponent<AllDictionaries>().dictionaryMouse;


    }

   

    //Function to change Controls (You have to apply the same controls in the EventControlScript of Shadow)
    public void ChangeControlOfPlayer()
    {

        this.GetComponent<ChangingMovementUi>().ChangeText();
        


        if (counterChange < 5)
        {
            ControlChanged.Invoke();
            if (counterChange == 0)
            {
                //ControlChanged.Invoke();
                whichTrigger = 0;
                myActionMap.RemoveAllBindingOverrides();
                //CancelInvoke();
                //counterChange += 1;
            }

            if (counterChange == 1)
            {
                //ControlChanged.Invoke();
                whichTrigger = 1;
                myActionMove.ApplyBindingOverride("<Keyboard>/x", null, "<Keyboard>/a");
                //CancelInvoke();
                //counterChange += 1;
            }

            if (counterChange == 2)
            {
               // ControlChanged.Invoke();
                whichTrigger = 2;
                myActionMove.ApplyBindingOverride("<Keyboard>/g", null, "<Keyboard>/d");
                //CancelInvoke();
                //counterChange += 1;
            }

            if (counterChange == 3)
            {
                //ControlChanged.Invoke();
                whichTrigger = 3;
                myActionJump.ApplyBindingOverride("<Keyboard>/m");
                //CancelInvoke();
                //counterChange += 1;
            }

            if (counterChange == 4)
            {
                //ControlChanged.Invoke();
                whichTrigger = 4;
                myActionFreeze.ApplyBindingOverride("<Keyboard>/p");
                //CancelInvoke();
                counterChange = -1;
            }
            counterChange += 1;
        }
    }
        /*

        //TriggerChange tc = other.gameObject.GetComponent<TriggerChange>();
        // Remove All overwritten Bindings
        if (tc.triggerID == 0)
        {
            ControlChanged.Invoke();
            whichTrigger = 0;
            myActionMap.RemoveAllBindingOverrides();
            CancelInvoke();
        }

        // Change Move A to X
        if (tc.triggerID == 1)
        {
            ControlChanged.Invoke();
            whichTrigger = 1;
            myActionMove.ApplyBindingOverride("<Keyboard>/x", null, "<Keyboard>/a");
            CancelInvoke();
        }

        //Change Move D to G
        if (tc.triggerID == 2)
        {
            ControlChanged.Invoke();
            whichTrigger = 2;
            myActionMove.ApplyBindingOverride("<Keyboard>/g", null, "<Keyboard>/d");
            CancelInvoke();
        }

        //Change Jump Space to M
        if (tc.triggerID == 3)
        {
            ControlChanged.Invoke();
            whichTrigger = 3;
            //myActionJump.ApplyBindingOverride("<Keyboard>/m", null, "<Keyboard>/space");
            myActionJump.ApplyBindingOverride("<Keyboard>/m");
            CancelInvoke();
        }

        //Change Freeze W to P
        if (tc.triggerID == 4)
        {
            ControlChanged.Invoke();
            whichTrigger = 4;
            //myActionFreeze.ApplyBindingOverride("<Keyboard>/p", null, "<Keyboard>/w");
            myActionFreeze.ApplyBindingOverride("<Keyboard>/p");
            CancelInvoke();
        }

        Respawn can't be changed, because you would have to change it in the Shadowspawner input
         
        //Change Respawn R to L
        if (tc.triggerID == 5)
        {
            ControlChanged.Invoke();
            whichTrigger = 5;
            //myActionRespawn.ApplyBindingOverride("<Keyboard>/enter", null, "<Keyboard>/r");
            myActionRespawn.ApplyBindingOverride("<Keyboard>/k");
            CancelInvoke();
        }
        //Change Move from A/D ??? to Pfeiltasten links/rechts
        if (tc.triggerID == 6)
        {
            ControlChanged.Invoke();
            whichTrigger = 6;
            myActionMove.ApplyBindingOverride("<Keyboard>/leftArrow", null, "<Keyboard>/a");
            myActionMove.ApplyBindingOverride("<Keyboard>/rightArrow", null, "<Keyboard>/d");
            CancelInvoke();
        }
        */
    

    private void OnTriggerStay2D(Collider2D other)
        {
            TriggerChange tc = other.gameObject.GetComponent<TriggerChange>();
            Debug.Log("entered");
            // Load Scene: ...
            
            if (tc.triggerID == 100)
            {
                SceneManager.LoadScene(sceneName: "Shady-Jump-Lvl.2");
            }

            if (tc.triggerID == 200)
            {
                SceneManager.LoadScene(sceneName: "NickFactory");
            }

            if (tc.triggerID == 300)
            {
                SceneManager.LoadScene(sceneName: "Main Menu");
            }

            if (tc.triggerID == 400)
            {
                SceneManager.LoadScene(sceneName: "Shady-Jump Lvl5");
            }

            if (tc.triggerID == 500)
            {
                SceneManager.LoadScene(sceneName: "Shady-Jump Lvl5");
            }

            if (tc.triggerID == 69)
            {
                SceneManager.LoadScene(sceneName: "Shady-Jump Lvl1");
            }

            //Debug.Log(myAction);
        }

}


