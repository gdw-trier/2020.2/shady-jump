﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class FlickeringLight : MonoBehaviour
{
	[SerializeField] private float flickerIntensity;
	[SerializeField] private float flickerSpeed;
	[SerializeField] private float startIntensity = 1.0f;

	[Range(0, 1)]
	[SerializeField]
	private float lightsOutChance = 0.05f;

	[SerializeField] private float lightsOutMinDuration = 0.2f;
	[SerializeField] private float lightsOutMaxDuration = 0.4f;

	private Light2D flickeringLight;

	private float randomOffset;

	private bool lightsOut;

	private void Awake()
	{
		randomOffset = Random.value * 100.0f;
		flickeringLight = GetComponent<Light2D>();
	}

	private void Update()
	{
		float lightIntensityModifier =
			(Mathf.PerlinNoise(Time.time * flickerSpeed + randomOffset, Time.time * flickerSpeed + randomOffset) -
			 0.5f) * 2 * flickerIntensity;
		
		float rand = Random.Range(0.0f, 1.0f);
		float intensity = 0.0f;
		
		if (!lightsOut)
		{
			if (rand >= lightsOutChance)
			{
				intensity = lightIntensityModifier + startIntensity;
			}
			else
			{
				float lightsOutTime = Random.Range(lightsOutMinDuration, lightsOutMaxDuration);
				intensity = 0.0f;
				lightsOut = true;
				Invoke(nameof(LightsOutCooldown), lightsOutTime);
			}
		}

		flickeringLight.intensity = intensity;
	}

	private void LightsOutCooldown()
	{
		lightsOut = false;
	}
}
