﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] public float speed;
    [SerializeField] private float jumpForce;
    [SerializeField] private float groundRayLength = 0.8f;
    [SerializeField] private float gravity = 1;
    [SerializeField] private float fallMultiplayer = 5f;
    [SerializeField] private float raycastOffset = 0.2f;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private LayerMask shadowLayer;
    [SerializeField] private float minYVelocity = -5;
    [SerializeField] private float jumpDelay = 0.15f;
    private float jumpTimer;
    private bool isJumpPressed;
    public bool left = false;
    private float dir;
    private InputActionsPlayer inputActions;
    private Action<InputAction.CallbackContext> jumpPerformedResponse;
    private Action<InputAction.CallbackContext> jumpCanceledResponse;
    private Animator anim;

    private Rigidbody2D rb;
    // Start is called before the first frame update
   /* private void OnMoveLeft(InputValue action)
    {
        dir = action.Get<float>();
        left = true;
    }
    private void OnMoveRight(InputValue action)
    {
        dir = action.Get<float>();
        left = false;
    }
    */

   public void OnMove(InputValue action)
   {
       dir = action.Get<float>();
        if (dir < -0.001)
        {
            left = true;
        }
        if (dir > 0.001)
        {
            left = false;
        }
        IsMoving();
    }

    /*private void OnJump(InputValue action)
    {
        Debug.Log("Jump");
        //isJumpPressed = action.Get<float>() > 0;
        if(isJumpPressed&& IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
        
    }*/
    
    private void ModifyPhysics()
    {
        if (IsGrounded())
        {
            rb.gravityScale = 0;
        }
        else
        {
            rb.gravityScale = gravity;
            if (rb.velocity.y < 0)
            {
                rb.gravityScale = gravity * fallMultiplayer;
            }else if(rb.velocity.y >0 && !isJumpPressed)
            {
                rb.gravityScale = gravity * (fallMultiplayer / 2);
            }
        }
        
    }
    private bool IsGrounded()
    {
        return Physics2D.Raycast(transform.position + new Vector3(raycastOffset, 0,0), Vector2.down, groundRayLength, groundLayer) || Physics2D.Raycast(transform.position - new Vector3(raycastOffset, 0,0), Vector2.down, groundRayLength, groundLayer) || Physics2D.Raycast(transform.position + new Vector3(raycastOffset, 0,0), Vector2.down, groundRayLength, shadowLayer) || Physics2D.Raycast(transform.position - new Vector3(raycastOffset, 0,0), Vector2.down, groundRayLength, shadowLayer) || Physics2D.Raycast(transform.position, Vector2.down, groundRayLength, shadowLayer);
    }
    private void Jump()
    {
        if(IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            jumpTimer = 0;
            FindObjectOfType<AudioManager>().Play("JumpSound");
        }
    }
    void Start()
    {
        anim = GetComponent<Animator>();

        rb = GetComponent<Rigidbody2D>();
        inputActions = new InputActionsPlayer();
        inputActions.Gameplay.Jump.Enable();
        jumpPerformedResponse = context =>
        {
            isJumpPressed = true;
            jumpTimer = Time.time + jumpDelay;
        };
        jumpCanceledResponse = context => isJumpPressed = false;
        inputActions.Gameplay.Jump.performed += jumpPerformedResponse;
        inputActions.Gameplay.Jump.canceled += jumpCanceledResponse;
    }
    private void FixedUpdate()
    {
        if (jumpTimer > Time.time)
        {
            Jump();
        }
        
        //TODO
        if(rb.velocity.y<= minYVelocity)
        {
        }
        rb.velocity = new Vector2(dir * speed, Math.Max(rb.velocity.y, minYVelocity));
        ModifyPhysics();
    }

    // Update is called once per frame
    void Update()
    {
        Left();
        IsMoving();
    }

    private void Left()
    {
        if (dir < -0.001)
        {
            anim.SetBool("Direction", true);
        }
        if (dir > 0.001)
        {
            anim.SetBool("Direction", false);
        }
    }

    private void IsMoving()
    {
        if (dir == 0)
        {
            anim.SetBool("Moving", false);
        }
        else
        {
            anim.SetBool("Moving", true);
        }
    }


    private void OnDisable()
    {
        if (inputActions == null) return;
        inputActions.Gameplay.Jump.performed -= jumpPerformedResponse;
        inputActions.Gameplay.Jump.canceled -= jumpCanceledResponse;
    }
}
