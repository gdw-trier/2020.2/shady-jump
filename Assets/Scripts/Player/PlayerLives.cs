﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class PlayerLives : MonoBehaviour
{
    public int lifes = 3;
    private Vector2 flashUp = new Vector2(1f,0.35f), flashMiddle = new Vector2(1, 0), flashDown = new Vector2(1, -0.35f);
    private Vector2 flashUpRay = new Vector2(3,1.5f), flashMiddleRay = new Vector2(3, 0), flashDownRay = new Vector2(3, -1.5f);
    public float flashDistance = 3;
    public int flashDamage = 10;
    public bool shadow = false;
    private bool flash = false;
    private Vector2 pos;
    private RaycastHit2D[] hitUp, hitMiddle, hitDown;
    private InputActionsPlayer inputActions;
    private Light2D flashlightLeft;
    private Light2D flashlightRight;
    private Material material;
    [SerializeField] private Transform flashlightTransformRight;
    [SerializeField] private Transform flashlightTransformLeft;
    private bool deathSoundPlayed = false;
    [SerializeField] private Canvas CanDeath;
    [SerializeField] private Canvas CanIngame;
    private static readonly int Power = Shader.PropertyToID("Vector1_53185279");

    private void Awake()
    {
        inputActions = new InputActionsPlayer();
        inputActions.Gameplay.Flashlight.Enable();
        inputActions.Gameplay.Flashlight.performed += context => flash = true;
        inputActions.Gameplay.Flashlight.canceled += context => flash = false;

        flashlightLeft = flashlightTransformLeft.gameObject.GetComponent<Light2D>();
        flashlightRight = flashlightTransformRight.gameObject.GetComponent<Light2D>();

        material = GetComponent<SpriteRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        bool left = this.GetComponent<PlayerMovement>().left;
        pos = this.transform.position;
        if(lifes == 0)
        {
            if(!deathSoundPlayed)
            {
                FindObjectOfType<AudioManager>().Play("PlayerDeath");
                deathSoundPlayed = true;
            }
            CanIngame.enabled = false;
            CanDeath.enabled = true;
        }
        // Debug.DrawRay(pos, ((left) ? -flashUpRay : flashUpRay), Color.white);
        // Debug.DrawRay(pos, ((left) ? -flashMiddleRay : flashMiddleRay), Color.white);
        // Debug.DrawRay(pos, ((left) ? -flashDownRay : flashDownRay), Color.white);
        CastRays(left);
       
    }

    private void CastRays(bool left)
    {
        if (flash)
        {
            hitUp = Physics2D.RaycastAll(pos, ((left) ? -flashUp : flashUp), flashDistance);
            DamageEnemy(hitUp);
            hitMiddle = Physics2D.RaycastAll(pos, ((left) ? -flashMiddle : flashMiddle), flashDistance);
            DamageEnemy(hitMiddle);
            hitDown = Physics2D.RaycastAll(pos, ((left) ? -flashDown : flashDown), flashDistance);
            DamageEnemy(hitDown);

            material.SetFloat(Power, 1.0f);
            
            if (left)
            {
                flashlightLeft.enabled = true;
                flashlightRight.enabled = false;
            }
            else
            {
                flashlightRight.enabled = true;
                flashlightLeft.enabled = false;
            }
        }
        else
        {
            flashlightLeft.enabled = false;
            flashlightRight.enabled = false;
            
            material.SetFloat(Power, 0.0f);
        }
    }

    private void DamageEnemy(RaycastHit2D[] hit)
    {
        string tags = (shadow == false) ? "EnemyShadow" : "EnemyPlayer";
        for (int i = 0; i < hit.Length; i++)
        {
            GameObject g = hit[i].collider.gameObject;
            if (g.tag == tags)
            {
                g.GetComponent<Enemy>().health -= flashDamage;
            }
        }
    }

    private void OnDrawGizmos()
    {
        var positionRight = flashlightTransformRight.position;
        var positionLeft = flashlightTransformLeft.position;
        
        Gizmos.DrawRay(positionRight, new Vector3(flashUp.x, flashUp.y, 0.0f).normalized * flashDistance);
        Gizmos.DrawRay(positionRight, new Vector3(flashMiddle.x, flashMiddle.y, 0.0f).normalized * flashDistance);
        Gizmos.DrawRay(positionRight, new Vector3(flashDown.x, flashDown.y, 0.0f).normalized * flashDistance);
        Gizmos.DrawRay(positionLeft, new Vector3(flashUp.x, flashUp.y, 0.0f).normalized * -flashDistance);
        Gizmos.DrawRay(positionLeft, new Vector3(flashMiddle.x, flashMiddle.y, 0.0f).normalized * -flashDistance);
        Gizmos.DrawRay(positionLeft, new Vector3(flashDown.x, flashDown.y, 0.0f).normalized * -flashDistance);
    }
}
