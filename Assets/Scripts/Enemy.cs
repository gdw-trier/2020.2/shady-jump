﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float checkForGroundLength = 2.0f, stepSize = 0.1f, chaseSpeed = 0.2f, enemySpeed = 2;
    public float waitForTurning = 4.0f, distanceForChasing = 3.0f, rayOffset = 2.5f;
    public int enemyLevel = 1;
    public LayerMask groundLayer;
    public GameObject player;
    public Transform shadow;
    public bool playerEnemy = true;
    public int health = 100;
    public float waitTilNextAttackTime = 5.0f;
    public float minimalDistance = 1.5f;

    private float direction = 1.0f;
    private Vector2 vector = new Vector2(1,-1), pos;
    private bool chasePlayer = false, attacked = false, coroute = false;
    private RaycastHit2D hit;
    private int layerMask;
    private float playerDistance, shadowDistance;
    private float time = 0;
    private Rigidbody2D rb;
    private Animator anim;


    // Start is called before the first frame update
    void Start()
    {
        pos = this.transform.position;
        if (playerEnemy)
        {
            layerMask = 1 << 9;
        }
        else
        {
            layerMask = 1 << 12;
        }
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            Destroy(gameObject);
        }

        if (shadow)
        {
            CheckForPlayer();
        }
        else
        {
            if (GameObject.FindGameObjectWithTag("Shadow"))
            {
                shadow = GameObject.FindGameObjectWithTag("Shadow").transform;
            }
        }

        if(chasePlayer == false)
        {
            EnemyMoveRandomly();
            time = 0;
            attacked = false;
        }
        else
        {
            time -= Time.deltaTime;
            if(playerEnemy)
            {
                EnemyChasePlayer();
                Attack();
            }
            else
            {
                if (!shadow)
                {
                    chasePlayer = false;
                }
                else
                {
                    EnemyChaseShadow();
                }

            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Shadow" && playerEnemy == false)
        {
            anim.SetTrigger("Attack");
            collision.gameObject.GetComponent<EventControlChange>().GetChangeOnPlayer();
            Destroy(collision.gameObject);
        }
    }

    IEnumerator ExampleCoroutine()
    {
        yield return new WaitForSeconds(1);
        playerDistance = Vector2.Distance(pos, player.transform.position);
        if (playerDistance < minimalDistance)
        {
            var playerLives = player.GetComponent<PlayerLives>();
            playerLives.lifes--;
            attacked = true;
            time = waitTilNextAttackTime;
            if (playerLives.lifes >= 0)
            {
                FindObjectOfType<AudioManager>().Play("PlayerDamage");
            }
        }
        coroute = false;
    }

    void Attack()
    {
        if (playerDistance < minimalDistance)
        {
            if (time < 0)
            {
                anim.SetTrigger("Attack");
                if(coroute == false)
                {
                    coroute = true;
                    StartCoroutine(ExampleCoroutine());
                }
            }
        }
        else if(chasePlayer == true && attacked == true)
        {
            if(time <= 0 || time >= 0.5f)
            {
                time = 0.5f;
            }
        }
    }

    void CheckForPlayer()
    {
        playerDistance = Vector2.Distance(pos, player.transform.position);
        shadowDistance = Vector2.Distance(pos, shadow.transform.position);
        if(chasePlayer == true)
        {
            direction *= (-1);
        }
        Vector2 side = new Vector2(1 * direction, 0);

        if(enemyLevel == 1)
        {
            Vector2 positon = new Vector2(pos.x + (rayOffset * direction), pos.y);
            hit = Physics2D.Raycast(positon, side, distanceForChasing);
        }
        else
        {
            int x = (this.transform.position.x >= player.transform.position.x) ? -1 : 1;
            Vector2 positon = new Vector2(pos.x + (rayOffset * x), pos.y);
            if (playerEnemy)
            {
                hit = Physics2D.Linecast(positon, player.transform.position, layerMask);
            }
            else
            {
                hit = Physics2D.Linecast(positon, shadow.transform.position, layerMask);
            }

        }
        
        if (hit)
        {
            if (playerEnemy)
            {
                chasePlayer = (hit.collider.gameObject.tag == "Player" && playerDistance < distanceForChasing) ? true : false;
            }
            else
            {
                chasePlayer = (hit.collider.gameObject.tag == "Shadow" && shadowDistance < distanceForChasing) ? true : false;
            }
        }
        else
        {
            chasePlayer = false;
        }
        if (playerEnemy)
        {
            Debug.DrawLine(pos, player.transform.position, Color.yellow);
        }
        else
        {
            Debug.DrawLine(pos, shadow.transform.position, Color.yellow);
        }

    }

    void EnemyMoveRandomly()
    {
        pos = this.transform.position;
        RaycastHit2D hit = Physics2D.Raycast(pos, new Vector2(1,-1), checkForGroundLength, groundLayer);
        RaycastHit2D hit2 = Physics2D.Raycast(pos, new Vector2(-1,-1), checkForGroundLength, groundLayer);
        RaycastHit2D hit3 = Physics2D.Raycast(pos, new Vector2(1,0), checkForGroundLength/2, groundLayer);
        RaycastHit2D hit4 = Physics2D.Raycast(pos, new Vector2(-1,0), checkForGroundLength/2, groundLayer);
        if (hit.collider == null || hit.collider.gameObject.tag != "Ground" || (hit3.collider != null && hit3.collider.gameObject.tag == "Wall"))
        {
            ChangeDirectionToLeft();
        }
        else if(hit2.collider == null || hit2.collider.gameObject.tag != "Ground" || (hit4.collider != null && hit4.collider.gameObject.tag == "Wall"))
        {
            ChangeDirectionToRight();
        }

        //Randomly change left after a certain number of seconds
        if (waitForTurning <= 0)
        {
            int x = Random.Range(0, 2);
            if(x >= 1)
            {
                ChangeDirectionToLeft();
            }
            waitForTurning = Random.Range(2, 7);
        }
        waitForTurning -= Time.deltaTime;

        Debug.DrawRay(pos, new Vector2(1,-1), Color.white);
        Debug.DrawRay(pos, new Vector2(-1,-1), Color.white);
        Debug.DrawRay(pos, new Vector2(1,0), Color.white);
        Debug.DrawRay(pos, new Vector2(-1,0), Color.white);
        rb.velocity = new Vector2(direction * enemySpeed, rb.velocity.y);
    }

    void EnemyChaseShadow()
    {
        vector.x = (this.transform.position.x > shadow.transform.position.x) ? -1 : 1;
        pos = this.transform.position;
        Debug.DrawRay(pos, vector, Color.white);
        // && (hit.collider.gameObject.tag == "Ground" && hit2.collider.gameObject.tag == "Ground")
        RaycastHit2D hit = Physics2D.Raycast(pos, new Vector2(1, -1), checkForGroundLength, groundLayer);
        RaycastHit2D hit2 = Physics2D.Raycast(pos, new Vector2(-1, -1), checkForGroundLength, groundLayer);

        if ((hit.collider != null && hit2.collider != null) || (shadow.transform.position.x < pos.x && hit2.collider != null) || (shadow.transform.position.x >= pos.x && hit.collider != null))
        {
            float chase = (pos.x >= shadow.transform.position.x) ? -chaseSpeed : chaseSpeed;
            if (chase > 0)
            {
                ChangeDirectionToLeft();
                anim.SetBool("Left", false);
            }
            else
            {
                ChangeDirectionToRight();
                anim.SetBool("Left", true);
            }
            if ((int)pos.x != (int)shadow.transform.position.x)
            {
                pos.x = pos.x + (-direction * chaseSpeed);
                this.transform.position = pos;
            }
        }
    }

    void EnemyChasePlayer()
    {
        vector.x = (this.transform.position.x > player.transform.position.x) ? -1 : 1;
        pos = this.transform.position;
        Debug.DrawRay(pos, vector, Color.white);
        RaycastHit2D hit = Physics2D.Raycast(pos, new Vector2(1, -1), checkForGroundLength, groundLayer);
        RaycastHit2D hit2 = Physics2D.Raycast(pos, new Vector2(-1, -1), checkForGroundLength, groundLayer);

        if ((hit.collider != null && hit2.collider != null) || (player.transform.position.x < pos.x && hit2.collider != null) || (player.transform.position.x >= pos.x && hit.collider != null))
        {
            float chase = (pos.x >= player.transform.position.x) ? -chaseSpeed : chaseSpeed;
            if(chase > 0)
            {
                ChangeDirectionToLeft();
                anim.SetBool("Left", false);
            }
            else
            {
                ChangeDirectionToRight();
                anim.SetBool("Left", true);
            }
            if((int)pos.x != (int)player.transform.position.x)
            {
                pos.x = pos.x + (-direction * chaseSpeed);
                this.transform.position = pos;
            }      
        }
    }

    void ChangeDirectionToRight()
    {
        stepSize = (stepSize > 0) ? stepSize : stepSize * (-1);
        vector.x = (vector.x > 0) ? vector.x : vector.x * (-1);
        direction = (direction > 0) ? direction : direction * (-1);
        anim.SetBool("Left", false);
    }

    void ChangeDirectionToLeft()
    {
        stepSize = (stepSize < 0) ? stepSize : stepSize * (-1);
        vector.x = (vector.x < 0) ? vector.x : vector.x * (-1);
        direction = (direction < 0) ? direction : direction * (-1);
        anim.SetBool("Left", true);
    }
}
