// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input/InputActionsSpawn.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActionsSpawn : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActionsSpawn()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActionsSpawn"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""5a323c62-94ff-47e6-a0df-c8f19e18464b"",
            ""actions"": [
                {
                    ""name"": ""ShadowRespawn"",
                    ""type"": ""Button"",
                    ""id"": ""0f7a74f7-7d95-42ed-b9b3-33be51dea978"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f073f22c-785e-4c4f-b70f-2370f0466d1f"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShadowRespawn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Gameplay
        m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
        m_Gameplay_ShadowRespawn = m_Gameplay.FindAction("ShadowRespawn", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Gameplay
    private readonly InputActionMap m_Gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_Gameplay_ShadowRespawn;
    public struct GameplayActions
    {
        private @InputActionsSpawn m_Wrapper;
        public GameplayActions(@InputActionsSpawn wrapper) { m_Wrapper = wrapper; }
        public InputAction @ShadowRespawn => m_Wrapper.m_Gameplay_ShadowRespawn;
        public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @ShadowRespawn.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShadowRespawn;
                @ShadowRespawn.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShadowRespawn;
                @ShadowRespawn.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShadowRespawn;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ShadowRespawn.started += instance.OnShadowRespawn;
                @ShadowRespawn.performed += instance.OnShadowRespawn;
                @ShadowRespawn.canceled += instance.OnShadowRespawn;
            }
        }
    }
    public GameplayActions @Gameplay => new GameplayActions(this);
    public interface IGameplayActions
    {
        void OnShadowRespawn(InputAction.CallbackContext context);
    }
}
