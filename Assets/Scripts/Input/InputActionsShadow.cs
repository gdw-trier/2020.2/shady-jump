// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input/InputActionsShadow.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActionsShadow : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActionsShadow()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActionsShadow"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""5a323c62-94ff-47e6-a0df-c8f19e18464b"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""8dfe7604-f8f3-4c1c-9471-d0f891c07f4b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FreezeShadow"",
                    ""type"": ""Button"",
                    ""id"": ""0f7a74f7-7d95-42ed-b9b3-33be51dea978"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Flashlight"",
                    ""type"": ""Button"",
                    ""id"": ""23567929-b522-4127-be09-8ed4d6bb27de"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""a9f18dbc-fed2-47ac-93fa-931c6e0bd075"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Menu"",
                    ""type"": ""Button"",
                    ""id"": ""bb20f7bf-8f4c-41d6-ae62-2e9df5dc0a8a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""4ef7e3d3-5150-47e4-b567-20d9be781f4d"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShadowRespawn"",
                    ""type"": ""Button"",
                    ""id"": ""a876a0e5-8ecc-4f6e-8ee3-e3d518b98c3a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b0ae32d1-467e-486f-b2f3-47841b92b795"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f073f22c-785e-4c4f-b70f-2370f0466d1f"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FreezeShadow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""db846a69-ba6a-498d-9e14-050ec72cda5a"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Flashlight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""57321c25-81ec-46ea-a9eb-8da128f59433"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Hold(duration=0.001)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""46ffe433-346f-4831-bfe5-519350228d7d"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""975f8b3e-9694-419e-b040-d216c4e2aed9"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""8b5aed8e-a243-4061-b4d0-4d1eaed052b4"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""e6410138-e978-4d62-a983-390a386b8e32"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""8c54beab-2ddf-45ec-91ae-773eb86bdc73"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShadowRespawn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Gameplay
        m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
        m_Gameplay_Interact = m_Gameplay.FindAction("Interact", throwIfNotFound: true);
        m_Gameplay_FreezeShadow = m_Gameplay.FindAction("FreezeShadow", throwIfNotFound: true);
        m_Gameplay_Flashlight = m_Gameplay.FindAction("Flashlight", throwIfNotFound: true);
        m_Gameplay_Jump = m_Gameplay.FindAction("Jump", throwIfNotFound: true);
        m_Gameplay_Menu = m_Gameplay.FindAction("Menu", throwIfNotFound: true);
        m_Gameplay_Move = m_Gameplay.FindAction("Move", throwIfNotFound: true);
        m_Gameplay_ShadowRespawn = m_Gameplay.FindAction("ShadowRespawn", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Gameplay
    private readonly InputActionMap m_Gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_Gameplay_Interact;
    private readonly InputAction m_Gameplay_FreezeShadow;
    private readonly InputAction m_Gameplay_Flashlight;
    private readonly InputAction m_Gameplay_Jump;
    private readonly InputAction m_Gameplay_Menu;
    private readonly InputAction m_Gameplay_Move;
    private readonly InputAction m_Gameplay_ShadowRespawn;
    public struct GameplayActions
    {
        private @InputActionsShadow m_Wrapper;
        public GameplayActions(@InputActionsShadow wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_Gameplay_Interact;
        public InputAction @FreezeShadow => m_Wrapper.m_Gameplay_FreezeShadow;
        public InputAction @Flashlight => m_Wrapper.m_Gameplay_Flashlight;
        public InputAction @Jump => m_Wrapper.m_Gameplay_Jump;
        public InputAction @Menu => m_Wrapper.m_Gameplay_Menu;
        public InputAction @Move => m_Wrapper.m_Gameplay_Move;
        public InputAction @ShadowRespawn => m_Wrapper.m_Gameplay_ShadowRespawn;
        public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @FreezeShadow.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnFreezeShadow;
                @FreezeShadow.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnFreezeShadow;
                @FreezeShadow.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnFreezeShadow;
                @Flashlight.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnFlashlight;
                @Flashlight.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnFlashlight;
                @Flashlight.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnFlashlight;
                @Jump.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                @Menu.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMenu;
                @Menu.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMenu;
                @Menu.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMenu;
                @Move.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @ShadowRespawn.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShadowRespawn;
                @ShadowRespawn.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShadowRespawn;
                @ShadowRespawn.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShadowRespawn;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @FreezeShadow.started += instance.OnFreezeShadow;
                @FreezeShadow.performed += instance.OnFreezeShadow;
                @FreezeShadow.canceled += instance.OnFreezeShadow;
                @Flashlight.started += instance.OnFlashlight;
                @Flashlight.performed += instance.OnFlashlight;
                @Flashlight.canceled += instance.OnFlashlight;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Menu.started += instance.OnMenu;
                @Menu.performed += instance.OnMenu;
                @Menu.canceled += instance.OnMenu;
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @ShadowRespawn.started += instance.OnShadowRespawn;
                @ShadowRespawn.performed += instance.OnShadowRespawn;
                @ShadowRespawn.canceled += instance.OnShadowRespawn;
            }
        }
    }
    public GameplayActions @Gameplay => new GameplayActions(this);
    public interface IGameplayActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnFreezeShadow(InputAction.CallbackContext context);
        void OnFlashlight(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnMenu(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnShadowRespawn(InputAction.CallbackContext context);
    }
}
