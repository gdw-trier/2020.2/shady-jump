﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;
using System.Data.SqlTypes;
using System.Runtime.CompilerServices;

public class IngameMenu : MonoBehaviour
{

    [SerializeField] private Canvas IngameOverlay;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Options;
    [SerializeField] private GameObject Lives;
    [SerializeField] private TextMeshProUGUI LiveDisplay;

    private void Start()
    {
        TakeDamage();
        Menu.enabled = false;
        Options.enabled = false;
    }

    private void Update()
    {
        TakeDamage();
    }

    private void OnMenu()
    {


        if(Menu.enabled == false && Options.enabled == false)
        {
            Menu.enabled = true;
            IngameOverlay.enabled = false;
            Time.timeScale = 0f;
        }
        else if(Menu.enabled == false && Options.enabled == true)
        {
            Options.enabled = false;
            Menu.enabled = true;
            Time.timeScale = 0f;
        }
        else if (Menu.enabled == true && Options.enabled == false)
        {
            Menu.enabled = false;
            IngameOverlay.enabled = true;
            Time.timeScale = 1f;
        }
        
    }
    
    public void OptionWindow()
    {
        if(Options.enabled == false)
        {
            Menu.enabled = false;
            Options.enabled = true;
        }
        else
        {
            Menu.enabled = true;
            Options.enabled = false;
        }
    }

    public void TakeDamage()
    {
       int FullLive = Lives.GetComponent<PlayerLives>().lifes;

        LiveDisplay.text = FullLive.ToString();
    }


    public void Play()
    {

        Time.timeScale = 1f;
        Options.enabled = false;
        Menu.enabled = false;

    }

}
