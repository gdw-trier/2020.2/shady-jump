﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;


public class ButtonBehavior : MonoBehaviour
{
    [SerializeField] private Canvas CanMain;
    [SerializeField] private Canvas CanOptions;
    [SerializeField] private Canvas CanCredits;

    private void Start()
    {
        CanOptions.enabled = false;
        CanCredits.enabled = false;
    }

    public void StartGame()
    {
        SceneManager.LoadScene(sceneName: "Starting scene") ;
    }

    public void Otions()
    {
        CanOptions.enabled = true;
        CanMain.enabled = false;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Credits()
    {
        CanCredits.enabled = true;
        CanMain.enabled = false;
    }

    public void Back()
    {
        SceneManager.LoadScene(sceneName:"Main Menu");
        Time.timeScale = 1f;
    }

    public void MenuBackOptions()
    {
        CanOptions.enabled = false;
        CanMain.enabled = true;
    }

    public void MenuBackCredits()
    {
        CanCredits.enabled = false;
        CanMain.enabled = true;
    }

}
