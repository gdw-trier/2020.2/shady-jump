﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.InputSystem.UI;
using TMPro;

public class ChangingMovementUi : MonoBehaviour
{

    private InputAction left;
    private InputAction right;
    private InputAction jump;
    private InputAction flashlight;
    private InputAction freezeShaddow;
    private InputAction shadowRespawn;
    [SerializeField] private TextMeshProUGUI TmpUp;
    [SerializeField] private TextMeshProUGUI TmpRight;
    [SerializeField] private TextMeshProUGUI TmpLeft;
    [SerializeField] private TextMeshProUGUI TmpFlashlight;
   // [SerializeField] private TextMeshProUGUI TmpInteract;
    [SerializeField] private TextMeshProUGUI TmpFreezeSaddow;
    [SerializeField] private TextMeshProUGUI TmpRespawn;

    void Start()
    {

        ChangeText();

    }

    public void ChangeText()
    {
        left = GetComponent<PlayerInput>().currentActionMap.FindAction("Move");
        right = GetComponent<PlayerInput>().currentActionMap.FindAction("Move");
        jump = GetComponent<PlayerInput>().currentActionMap.FindAction("Jump");
        flashlight = GetComponent<PlayerInput>().currentActionMap.FindAction("Flashlight");
        shadowRespawn = GetComponent<PlayerInput>().currentActionMap.FindAction("ShadowRespawn");
        freezeShaddow = GetComponent<PlayerInput>().currentActionMap.FindAction("FreezeShadow");

        string hLeft = left.GetBindingDisplayString();
        string hRight = right.GetBindingDisplayString();
        string hJump = jump.GetBindingDisplayString();
        string hFlashlight = flashlight.GetBindingDisplayString();
        string hRespawn = shadowRespawn.GetBindingDisplayString();
        string hfreezeShaddow = freezeShaddow.GetBindingDisplayString();

        if (hJump.Contains("Hold LEER"))
        {
            TmpUp.text = hJump.Substring(5);
        }
        else
        {
            TmpUp.text = hJump;
        }
        
        TmpLeft.text = hLeft.Substring(0, hLeft.Length -10);
       // Debug.Log(hRight);
       // Debug.Log(hLeft);
        TmpRight.text = hRight.Substring(9);
        TmpFlashlight.text = hFlashlight;
        TmpFreezeSaddow.text = hfreezeShaddow;
        TmpRespawn.text = hRespawn;
    }

}
