﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RisingLava : MonoBehaviour
{
    public float lavaSpeed = 0.01f;
    public string GameOverScene;
    public float leftDistance = 11.8f;
    private Transform cam;
    private Vector3 pos, check;
    private SpriteRenderer sprite;
    private float centerToRightDist;
    [SerializeField] private Canvas CanDeath;
    [SerializeField] private Canvas CanIngame;
    [SerializeField] private bool MovingLava;

    void Start()
    {
        pos = this.transform.position;
        check = this.transform.position;
        cam = Camera.main.transform;
        sprite = GetComponent<SpriteRenderer>();
        centerToRightDist = sprite.bounds.extents.x;
        // Debug.Log("Distance: " + 6 * centerToRightDist);
    }

    void Update()
    {
        if (MovingLava == true)
        {
            ShouldMove();
        }

        float dist = cam.position.x - this.transform.position.x;
        Debug.DrawLine(this.transform.position, cam.position, Color.green);
        if (dist >= leftDistance)
        {
            Vector2 rightPos = new Vector2(pos.x + 12*centerToRightDist, this.transform.position.y);
            pos.x += 12 * centerToRightDist;
            this.transform.position = pos;
        }
        else if (dist <= -leftDistance)
        {
            Vector2 leftPos = new Vector2(pos.x + (-(12*centerToRightDist)), this.transform.position.y);
            pos.x -= 12 * centerToRightDist;
            this.transform.position = pos;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //this.transform.position = check;
            //pos = check;
            FindObjectOfType<AudioManager>().Play("PlayerDeath");
            CanDeath.enabled = true;
            CanIngame.enabled = false;
            Time.timeScale = 0f;
        }
        else if (collision.gameObject.tag == "Shadow")
        {
            
            collision.gameObject.GetComponent<EventControlChange>().GetChangeOnPlayer();
            Destroy(collision.gameObject);
        }
    }

    private void ShouldMove()
    {
        pos.y += lavaSpeed;
        this.transform.position = pos;
    }
}
