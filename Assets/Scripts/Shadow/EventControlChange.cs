﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class EventControlChange : MonoBehaviour
{
    //private event Action cc;

    // Start is called before the first frame update
    private PlayerTriggerChange ptc;

    private void OnEnable()
    {
        GetComponentInParent<PlayerTriggerChange>().ControlChanged += ShadowControlChange;
    }

    void Start()
    {
        
        ptc = GetComponentInParent<PlayerTriggerChange>();
    }

    public void GetChangeOnPlayer()
    {
        ptc.ChangeControlOfPlayer();
    }
    private void ShadowControlChange()
    {
        var counter = ptc.counterChange;
        //Remove All Overwritten Bindings
        if (counter == 0)
        {
            
            var sActionMap = GetComponent<PlayerInput>().currentActionMap;
            sActionMap.RemoveAllBindingOverrides();
        }
        //Change Move A to X
        if (counter == 1)
        {
            var sAction = GetComponent<PlayerInput>().currentActionMap.FindAction("Move");
            sAction.ApplyBindingOverride("<Keyboard>/x",null, "<Keyboard>/a");
        }
        //Change Move D to G
        if (counter == 2)
        {
            var sAction = GetComponent<PlayerInput>().currentActionMap.FindAction("Move");
            sAction.ApplyBindingOverride("<Keyboard>/g",null, "<Keyboard>/d");
        }
        //Change Jump Space to M
        if (counter == 3)
        {
            var sAction = GetComponent<PlayerInput>().currentActionMap.FindAction("Jump");
            sAction.ApplyBindingOverride("<Keyboard>/m");
            //sAction.ApplyBindingOverride("<Keyboard>/m",null, "<Keyboard>/space");
        }
        // Change FreezeShadow W to P
        if (counter == 4)
        {
            var sAction = GetComponent<PlayerInput>().currentActionMap.FindAction("FreezeShadow");
            sAction.ApplyBindingOverride("<Keyboard>/p");
            //sAction.ApplyBindingOverride("<Keyboard>/p",null, "<Keyboard>/w");
        }
        //Destroy(gameObject);
    }
        // Change Respawn R to L
        /*if (ptc.whichTrigger == 5)
        {
            var sAction = GetComponent<PlayerInput>().currentActionMap.FindAction("ShadowRespawn");
            sAction.ApplyBindingOverride("<Keyboard>/k");
            //sAction.ApplyBindingOverride("<Keyboard>/l",null, "<Keyboard>/r");
        }
        // Change Move A/D zu Pfeiltasten links/rechts
        if (ptc.whichTrigger == 6)
        {
            var sAction = GetComponent<PlayerInput>().currentActionMap.FindAction("Move");
            sAction.ApplyBindingOverride("<Keyboard>/leftArrow",null, "<Keyboard>/a");
            sAction.ApplyBindingOverride("<Keyboard>/rightArrow",null, "<Keyboard>/d");
        }*/
    
    
    private void OnDisable()
    {
        
        if (GetComponentInParent<PlayerTriggerChange>() != null)
        {
            //GetComponentInParent<PlayerTriggerChange>().ChangeControlOfPlayer();
            GetComponentInParent<PlayerTriggerChange>().ControlChanged -= ShadowControlChange;
        }

    }
    
}
