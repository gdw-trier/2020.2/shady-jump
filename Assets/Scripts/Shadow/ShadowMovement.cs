﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShadowMovement : MonoBehaviour
{
	[SerializeField] public float speed;
	[SerializeField] private float jumpForce;
	[SerializeField] private float groundRayLength = 0.8f;
	[SerializeField] private float gravity = 1;
	[SerializeField] private float fallMultiplayer = 5f;
	[SerializeField] private float raycastOffset = 0.2f;
	[SerializeField] private LayerMask groundLayer;
	[SerializeField] private LayerMask playerLayer;
	[SerializeField] private float minYVelocity = -5;
	[SerializeField] private float jumpDelay = 0.15f;
	private float jumpTimer;
	private bool isJumpPressed;
	private bool playerOntop;
	private Action<InputAction.CallbackContext> jumpPerformedResponse;
	private Action<InputAction.CallbackContext> jumpCanceledResponse;
	private bool freeze = false;
	private float dir;
	private InputActionsShadow inputActions;
	private Animator anim;
	private Rigidbody2D rb;

	// Start is called before the first frame update
	/*private void OnMoveLeft(InputValue action)
	 {
		 dir = action.Get<float>();
	 }
	 private void OnMoveRight(InputValue action)
	 {
		 dir = action.Get<float>();
	 }*/
	public void OnMove(InputValue action)
	{
		dir = action.Get<float>();
		IsMoving();
	}

	// private void OnJump(InputValue action)
	// {
	//     if (!IsPlayerOnTop())
	//     {
	//         Debug.Log("Jump");
	//         isJumpPressed = action.Get<float>() > 0;
	//         if (isJumpPressed && IsGrounded())
	//         {
	//             rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
	//         }
	//     }
	//     //TODO
	//     else
	//     {
	//         Debug.Log("You are too heavy!");
	//     }
	//
	//
	// }

	private bool IsGrounded()
	{
		return Physics2D.Raycast(transform.position + new Vector3(raycastOffset, 0, 0), Vector2.down, groundRayLength,
			groundLayer) || Physics2D.Raycast(transform.position - new Vector3(raycastOffset, 0, 0), Vector2.down,
			groundRayLength, groundLayer);
	}

	private void Jump()
	{
		if (!IsPlayerOnTop())
		{
			if (IsGrounded())
			{
				rb.velocity = new Vector2(rb.velocity.x, 0);
				rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
				jumpTimer = 0;
			}
		}
		else
		{
			jumpTimer = 0;
		}
	}

	private void ModifyPhysics()
	{
		if (IsGrounded())
		{
			rb.gravityScale = 0;
		}
		else
		{
			rb.gravityScale = gravity;
			if (rb.velocity.y < 0)
			{
				rb.gravityScale = gravity * fallMultiplayer;
			}
			else if (rb.velocity.y > 0 && !isJumpPressed)
			{
				rb.gravityScale = gravity * (fallMultiplayer / 2);
			}
		}
	}

	private bool IsPlayerOnTop()
	{
		Vector3 position = transform.position;
		bool left = Physics2D.Raycast(position - new Vector3(raycastOffset + 0.0001f, 0, 0), Vector2.up,
			groundRayLength * 1.1f, playerLayer);
		bool right = Physics2D.Raycast(position + new Vector3(raycastOffset - 0.0001f, 0, 0), Vector2.up,
			groundRayLength * 1.1f, playerLayer);

		return left || right;
	}

	void Start()
	{
		anim = GetComponent<Animator>();

		rb = GetComponent<Rigidbody2D>();
		inputActions = new InputActionsShadow();
		inputActions.Gameplay.Jump.Enable();
		jumpPerformedResponse = context =>
		{
			isJumpPressed = true;
			jumpTimer = Time.time + jumpDelay;
		};
		jumpCanceledResponse = context => isJumpPressed = false;
		inputActions.Gameplay.Jump.performed += jumpPerformedResponse;
		inputActions.Gameplay.Jump.canceled += jumpCanceledResponse;
	}

	private void FixedUpdate()
	{
		if (jumpTimer > Time.time)
		{
			Jump();
		}

		if (rb.velocity.y <= minYVelocity)
		{
		}

		rb.velocity = new Vector2(dir * speed, Math.Max(rb.velocity.y, minYVelocity));
		ModifyPhysics();
	}

	// Update is called once per frame
	public void Update()
	{
		Left();
		IsMoving();
		IsFrozen();
	}

	private void Left()
	{
		if (dir < -0.001)
		{
			anim.SetBool("Direction", true);
		}

		if (dir > 0.001)
		{
			anim.SetBool("Direction", false);
		}
	}

	private void IsMoving()
	{
		if (dir == 0)
		{
			anim.SetBool("Moving", false);
		}
		else
		{
			anim.SetBool("Moving", true);
		}
	}

	public void OnFreezeShadow()
	{
		if (freeze == false)
		{
			freeze = true;
		}
		else
		{
			freeze = false;
		}
	}

	private void IsFrozen()
	{
		if (freeze == true)
			anim.SetBool("Frozen", true);
		else
		{
			anim.SetBool("Frozen", false);
		}
	}

	private void OnDisable()
	{
		if (inputActions == null) return;
		inputActions.Gameplay.Jump.performed -= jumpPerformedResponse;
		inputActions.Gameplay.Jump.canceled -= jumpCanceledResponse;
	}
}
