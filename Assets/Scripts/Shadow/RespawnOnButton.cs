﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RespawnOnButton : MonoBehaviour
{
    //private Health h;
    private GameObject go;
    [SerializeField] private Transform shadowSpawner;
    [SerializeField] private GameObject shadowPrefab;
    [SerializeField]private GameObject currentShadow;
    private Vector3 leftSpawn;
    private bool lookLeft;
    private PlayerMovement pm;
    private float dist;
    private float pos1;
    private float pos2;
    private float pos3;
    private float pos4;
    private float doubleDis;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Transform playerTransform;

    private void Start()
    {
        pm = GetComponentInParent<PlayerMovement>();
        //dist = Math.Abs(GetComponent<Transform>().position.x - GetComponentInParent<Transform>().position.x);
        dist = GetComponent<Transform>().localPosition.x;
        pos4 = dist * 1.5f;
        pos3 = dist * 0.75f;
        pos2 = dist * 0.5f;
        pos1 = dist * 0.25f;
        doubleDis = dist * 2;
        //Debug.Log(dist);
    }
    
    private int checkRight()
    {
        if(Physics2D.Raycast(playerTransform.position, Vector2.right,pos1, groundLayer))
        {
            return 1;
        }
        else
        {
            if(Physics2D.Raycast(playerTransform.position, Vector2.right,pos2, groundLayer))
            {
                return 2;
            }
            else
            {
                if(Physics2D.Raycast(playerTransform.position, Vector2.right,pos3, groundLayer))
                {
                    return 3;
                }
                else
                {
                    if(Physics2D.Raycast(playerTransform.position, Vector2.right,dist, groundLayer))
                    {
                        return 4;
                    }
                    else
                    {
                        return 5;
                    }
                }
            }
        }
        
    }
    
    private int checkLeft()
    {
        if(Physics2D.Raycast(playerTransform.position, Vector2.left,pos1, groundLayer))
        {
            return 1;
        }
        else
        {
            if(Physics2D.Raycast(playerTransform.position, Vector2.left,pos2, groundLayer))
            {
                return 2;
            }
            else
            {
                if(Physics2D.Raycast(playerTransform.position, Vector2.left,pos3, groundLayer))
                {
                    return 3;
                }
                else
                {
                    if(Physics2D.Raycast(playerTransform.position, Vector2.left,dist, groundLayer))
                    {
                        return 4;
                    }
                    else
                    {
                        return 5;
                    }
                }
            }
        }
        
    }
    private void OnShadowRespawn(InputValue value)
    {
        lookLeft = pm.left;
        leftSpawn = shadowSpawner.position - new Vector3(doubleDis, 0, 0);
        Destroy(currentShadow.gameObject);
        if (lookLeft)
        { 
            if (checkLeft() == 5)
            {
                go = Instantiate(shadowPrefab, leftSpawn, Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            
            if (checkLeft() == 4)
            {
                go = Instantiate(shadowPrefab, (leftSpawn+ new Vector3(pos1,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            if (checkLeft() == 3)
            {
                go = Instantiate(shadowPrefab, (leftSpawn+ new Vector3(pos2,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            if (checkLeft() == 2)
            {
                go = Instantiate(shadowPrefab, (leftSpawn+ new Vector3(pos3,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            if (checkLeft() == 1)
            {
                go = Instantiate(shadowPrefab, (leftSpawn+ new Vector3(pos4,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            //go = Instantiate(shadowPrefab, leftSpawn, Quaternion.identity, gameObject.transform);
            //currentShadow = go; 
        }
        else
        {
            if (checkRight() == 5)
            {
                go = Instantiate(shadowPrefab, shadowSpawner.position, Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            
            if (checkRight() == 4)
            {
                go = Instantiate(shadowPrefab, (shadowSpawner.position- new Vector3(pos1,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            if (checkRight() == 3)
            {
                go = Instantiate(shadowPrefab, (shadowSpawner.position- new Vector3(pos2,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            if (checkRight() == 2)
            {
                go = Instantiate(shadowPrefab, (shadowSpawner.position- new Vector3(pos3,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
            if (checkRight() == 1)
            {
                go = Instantiate(shadowPrefab, (shadowSpawner.position- new Vector3(pos4,0,0)), Quaternion.identity, gameObject.transform);
                currentShadow = go;
            }
        }
    }
}
