﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShadowInteraction : MonoBehaviour
{
    private Rigidbody2D rb;
    

    private bool isFreezed = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnFreezeShadow(InputValue value)
    {
        if (!isFreezed)
        {
           // Debug.Log("Freeze");
           rb.constraints = RigidbodyConstraints2D.FreezeAll;
           isFreezed = true;
        }
        else
        {
            // Debug.Log("Unfreeze");
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            isFreezed = false;
        }
        
    }
}
